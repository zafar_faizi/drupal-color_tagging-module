<?php
/**
 * $image - contains the image html rendered by Drupal
 * $color_tagging_image_palette - contains the image color palette
 */
?>
<?php print $image; ?>
<div class="color-tagging-image-palette">
    <?php if (isset($color_tagging_image_palette['dominant'])) { ?>
        <ul class="color-tagging-colors-wrapper">
            <li>Dominant Color:</li>
            <li>
                <div class="color-tagging-actual-color" style="background-color: <?php echo $color_tagging_image_palette['dominant']; ?>"></div>
                <div class="color-tagging-color-name">
                    <p class="color-tagging-color-text"><?php echo closest_match_color_name($color_tagging_image_palette['dominant']); ?></p>
                    <p class="color-tagging-color-code">(<?php echo $color_tagging_image_palette['dominant']; ?>)</p>
                </div>
            </li>
        </ul>
    <?php } ?>

    <ul class="color-tagging-colors-wrapper">
        <?php foreach (explode(',', $color_tagging_image_palette['palette']) as $color_tagging_color) { ?>
            <li>
                <div class="color-tagging-actual-color" style="background-color: <?php echo $color_tagging_color; ?>"></div>
                <div class="color-tagging-color-name">
                    <p class="color-tagging-color-text"><?php echo closest_match_color_name($color_tagging_color); ?></p>
                    <p class="color-tagging-color-code">(<?php echo $color_tagging_color; ?>)</p>
                </div>
            </li>
        <?php } ?>
        <?php if (isset($color_tagging_image_palette['full_color'])) { ?>
            <li>
                <div class="color-tagging-actual-color" style="background-color: red"></div>
                <div class="color-tagging-color-name">
                    <p class="color-tagging-color-text">Full Color</p>
                    <p class="color-tagging-color-code">(R-G-B)</p>
                </div>
            </li>
        <?php } ?>
        <?php if (isset($color_tagging_image_palette['black_white'])) { ?>
            <li>
                <div class="color-tagging-actual-color" style="background-color: #000;"></div>
                <div class="color-tagging-color-name">
                    <p class="color-tagging-color-text">Black & White</p>
                    <p class="color-tagging-color-code">(#000000)</p>
                </div>
            </li>
        <?php } ?>
        <?php if (isset($color_tagging_image_palette['transparent'])) { ?>
            <li>
                <div class="color-tagging-actual-color" style="background-color: #FFFFFF;"></div>
                <div class="color-tagging-color-name">
                    <p class="color-tagging-color-text">Transparent</p>
                    <p class="color-tagging-color-code">(#------)</p>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>
